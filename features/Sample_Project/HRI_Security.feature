Feature: Security Scenarios for HRI

  @SecSc6
  Scenario: HRI application access to HUB_HRI_Admin Analyst
    Given   User opens AF URL in QAI region
    When User clicks on Administration Link
    And  User Clicks on Manage Adminsitrators
    And User Search for "Kumar, Shashank" and clicks on Find Now
    And User clicks on the Name
    And User clicks on Edit button under Roles
    And User selects HUB_HRI_Config. Read_Only_Role and click on save
    And User open Benefit Hub
    And Verify that the correct clients are displayed
    And User click on Client name to navigate to the homepage
    And Check that the user is only able to view File configuration list
    And Check that the user is able to view "File configuration" checklist page
    And Check if the user views the file configuration then he cannot edit anything
    And Check if the user can view the Data mapping page for both SDLF and Cutom files
    And Check if the user can view the 'Valid values and decode' screen
    And Check if the user can view the 'effective date derivation' screen
    And Check if the user can view the 'Data validation' page
    And Check if the user can view the 'Data derivation' screen
    And Check that the user do not have selection on Administration tab
    And Check the user is able to view the Event trigger configuration
    And Verify that The Add button on the extreme right to 'Add from Library' and 'Add a New Client Specific Group' is not visible
    And Verify that the Promote Button is visible and should be disabled
    And User assigns the same role for scenario 6

  @SecSc5
  Scenario: HRI application access to HUB_HRI_Config.Promote_PROD
    Given   User opens AF URL in QAI region
    When User clicks on Administration Link
    And  User Clicks on Manage Adminsitrators
    And User Search for "Kumar, Shashank" and clicks on Find Now
    And User clicks on the Name
    And User clicks on Edit button under Roles
    And User selects HUB_HRI_Config.Promote_PROD_Role and click on save
    And User open Benefit Hub
    And Verify that the correct clients are displayed
    And User click on Client name to navigate to the homepage
    And Check that the user is only able to view File configuration list
    And Check that the user is able to view "File configuration" checklist page
    And Check if the user views the file configuration then he cannot edit anything
    And Check if the user can view the Data mapping page for both SDLF and Cutom files
    And Check if the user can view the 'Valid values and decode' screen
    And Check if the user can view the 'effective date derivation' screen
    And Check if the user can view the 'Data validation' page
    And Check if the user can view the 'Data derivation' screen
    And Check that the user do not have selection on Administration tab
    And Check the user is able to view the Event trigger configuration
    And Verify that The Add button on the extreme right to 'Add from Library' and 'Add a New Client Specific Group' is not visible
    And Verify that the Promote Button is visible
    And User click on the Promote button and promote between environment
    And User assigns the same role  for scenario 5

    @SecSc4
    Scenario: HRI application access to HUB_HRI_Config.Client_File_Setup Role
      Given   User opens AF URL in QAI region
      When User clicks on Administration Link
      And  User Clicks on Manage Adminsitrators
      And User Search for "Kumar, Shashank" and clicks on Find Now
      And User clicks on the Name
      And User clicks on Edit button under Roles
      And User selects HUB_HRI_Config.Client_File_Setup Role and click on save
      And User open Benefit Hub
     And Verify that the correct clients are displayed
      And User click on Client name to navigate to the homepage
      And User clicks on HR Import Link
      And Check if the user can view the Data mapping page for both SDLF and Custom files
      And Check that the user do not have selection on Administration tab
      And Verify that the Promote Button is visible
      And User click on the Promote button and promote between environment
       And User assigns the same role  for scenario 4

  @SecSc3
  Scenario: HRI application access to HUB_HRI_Admin.Read_Only
    Given   User opens AF URL in QAI region
    When User clicks on Administration Link
    And  User Clicks on Manage Adminsitrators
    And User Search for "Kumar, Shashank" and clicks on Find Now
    And User clicks on the Name
    And User clicks on Edit button under Roles
    And User selects HUB_HRI_Admin.Read_Only Role and click on save
    And User open Benefit Hub
    And Verify that the correct clients are displayed
    And User click on Client name to navigate to the homepage
    And Check that the user is not able to view any configuration component
    And Check that the user is able to view File Dashboard
    And Check that the user can view the File breakdown page
    And User assigns the same role  for scenario 3

  @SecSc2
  Scenario: HRI application access to HUB_HRI_Admin.Manager
    Given   User opens AF URL in QAI region
    When User clicks on Administration Link
    And  User Clicks on Manage Adminsitrators
    And User Search for "Kumar, Shashank" and clicks on Find Now
    And User clicks on the Name
    And User clicks on Edit button under Roles
    And User selects HUB_HRI_Admin.Manager role and click on save
    And User open Benefit Hub
    And Verify that the correct clients are displayed
    And User click on Client name to navigate to the homepage
    And Check that the user is not able to view any configuration component
    And Check that the user can view File Dashboard File breakdown and do error remediation and view Participant List tab
    And User is able to remediate the file
    And User assigns the same role  for scenario 2

  @SecSc1
  Scenario: HRI application access to HUB_HRI_Admin.Analyst
    Given   User opens AF URL in QAI region
    When User clicks on Administration Link
    And  User Clicks on Manage Adminsitrators
    And User Search for "Kumar, Shashank" and clicks on Find Now
    And User clicks on the Name
    And User clicks on Edit button under Roles
    And User selects HUB_HRI_Admin.Analyst role and click on save
    And User should see confirmation message
    And User open Benefit Hub
    And Verify that the correct clients are displayed
    And User click on Client name to navigate to the homepage
    And Check that the user is not able to view any configuration component
    And Check that the user can view File Dashboard File breakdown and do error remediation and view Participant List tab
    And User is able to remediate the file
    And User assigns the same role  for scenario 1

    @Dummy

    Scenario: This is a dummy function
    Given User opens AF URL in QAI region
    When User clicks on Administration Link
    And  User Clicks on Manage Adminsitrators
    And User Search for "Kumar, Shashank" and clicks on Find Now
    And User clicks on the Name
    And User clicks on Edit button under Roles
    And User selects HUB_HRI_Admin.Analyst role
      And User clicks on Save Button
    And User should see confirmation message

      @Before_Running_Test
      Scenario: User removes the user from QA-Analyst-TEST CLIENTS and QA-TL/Mgr-All except MMC groups
        Given User opens AF URL in QAI region
        When User clicks on Administration Link
        And  User Clicks on Manage Adminsitrators
        And User Search for "Kumar, Shashank" and clicks on Find Now
        And User clicks on the Name
        And User clicks on Edit button under Groups
        And User clicks on QA-Analyst-TEST-CLIENTS group
        And User clicks on QA-TL or Mgr-All except MMC group
        And User clicks on Save Button for Group
        And User should see confirmation message