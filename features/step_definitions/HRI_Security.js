var data = require('../../TestResources/HRI_Test_data.js');
var Objects = require(__dirname + '/../../repository/HRI_Pages.js');
var action = require(__dirname + '/../../features/step_definitions/ReusableTests.js');
var fs = require('fs');
var Administration_Foundation , browser;
function initializePageObjects(browser, callback) {
    var HPage = browser.page.HRI_Pages();
    Administration_Foundation = HPage.section.Administration_Foundation;
    callback();
}

module.exports = function() {

    this.Given(/^User opens AF URL in QAI region$/, function () {
browser=this;
        var AFURL;
     //   AFURL = data.AFURL;
         initializePageObjects(browser, function () {
             browser.maximizeWindow()
                 .deleteCookies()
                 .url(data.AFURL);
             browser.timeoutsImplicitWait(30000);
         });

    });

    this.When(/^User clicks on Administration Link$/, function () {
        Administration_Foundation.waitForElementVisible('@lnk_Administration',data.wait)
            .click('@lnk_Administration')
        });
    this.When(/^User Clicks on Manage Adminsitrators$/, function () {
        Administration_Foundation.waitForElementVisible('@lnk_Manage_Administrator',data.wait)
            .click('@lnk_Manage_Administrator')
    });
    this.When(/^User Search for "Kumar, Shashank" and clicks on Find Now$/, function () {
        Administration_Foundation.waitForElementVisible('@lnk_text_box',data.wait)
            .setValue('@lnk_text_box',data.Name);
        Administration_Foundation.waitForElementVisible('@lnk_find_now',data.wait)
            .click('@lnk_find_now');
    });
    this.When(/^User clicks on the Name$/, function () {
        Administration_Foundation.waitForElementVisible('@lnk_User_Name',data.wait)
            .click('@lnk_User_Name')
    });
    this.When(/^User clicks on Edit button under Roles$/, function () {
        Administration_Foundation.waitForElementVisible('@lnk_Role_edit',data.wait)
            .click('@lnk_Role_edit')
            });
    this.When(/^User selects HUB_HRI_Config. Read_Only_Role and click on save$/, function () {
        Administration_Foundation.waitForElementVisible('@lnk_HUB_HRI_Config_Read_Only_Role',data.wait)
            .click('@lnk_HUB_HRI_Config_Read_Only_Role');
        Administration_Foundation.waitForElementVisible('@lnk_save_role',data.wait)
            .click('@lnk_save_role');
    });

    this.When(/^User open Benefit Hub$/, function () {
        browser=this;
        var BHURL;

            browser.maximizeWindow()
                .deleteCookies()
                .url(data.BHURL);
            browser.timeoutsImplicitWait(60000);
            });


    this.When(/^Verify that the correct clients are displayed$/, function () {
        Administration_Foundation.waitForElementVisible('@lnk_client_AvonforMBCDOM', data.wait)
            .waitForElementVisible('@lnk_client_AvonforMBCDOM',data.wait);
        Administration_Foundation.waitForElementVisible('@lnk_client_BeneCalcTestClient', data.wait)
            .waitForElementVisible('@lnk_client_BeneCalcTestClient',data.wait);
        Administration_Foundation.waitForElementVisible('@lnk_client_MercerMarketplace365Plus', data.wait)
            .waitForElementVisible('@lnk_client_MercerMarketplace365Plus',data.wait);
        Administration_Foundation.waitForElementVisible('@lnk_client_QAClientOne', data.wait)
            .waitForElementVisible('@lnk_client_QAClientOne',data.wait);
        Administration_Foundation.waitForElementVisible('@lnk_client_QAStandard', data.wait)
            .waitForElementVisible('@lnk_client_QAStandard',data.wait);
        Administration_Foundation.waitForElementVisible('@lnk_client_QAACME', data.wait)
            .waitForElementVisible('@lnk_client_QAACME',data.wait);
        Administration_Foundation.waitForElementVisible('@lnk_client_QAHBACME', data.wait)
            .waitForElementVisible('@lnk_client_QAHBACME',data.wait);
        Administration_Foundation.waitForElementVisible('@lnk_client_QAWORKTESTDROPHB', data.wait)
            .waitForElementVisible('@lnk_client_QAWORKTESTDROPHB',data.wait);
        Administration_Foundation.waitForElementVisible('@lnk_client_SLSDM', data.wait)
            .waitForElementVisible('@lnk_client_SLSDM',data.wait);

    });

    this.When(/^User click on Client name to navigate to the homepage$/, function () {
        Administration_Foundation.waitForElementVisible('@lnk_client_QAACME',data.wait)
            .click('@lnk_client_QAACME')
            });
    this.When(/^Check that the user is only able to view File configuration list$/, function () {
        Administration_Foundation.waitForElementVisible('@lnk_Configuration',data.wait)
            .click('@lnk_Configuration')
    });
    this.When(/^Check that the user is able to view "File configuration" checklist page$/, function () {
        Administration_Foundation.waitForElementVisible('@lnk_HRImport_Link',data.wait)
            .click('@lnk_HRImport_Link')
    });
    this.When(/^Check if the user views the file configuration then he cannot edit anything$/, function () {
        Administration_Foundation.waitForElementVisible('@lnk_ACME_0100',data.wait)
            .click('@lnk_ACME_0100')
        Administration_Foundation.waitForElementVisible('@lnk_file_config_edit',data.wait)
            .click('@lnk_file_config_edit')
        Administration_Foundation.waitForElementVisible('@lnk_Inbound_File_Type',data.wait)
            .click('@lnk_Inbound_File_Type')
        Administration_Foundation.waitForElementVisible('@lnk_Close_Button',data.wait)
            .click('@lnk_Close_Button')

    });
    this.When(/^Check if the user can view the Data mapping page for both SDLF and Cutom files$/, function () {
        Administration_Foundation.waitForElementVisible('@lnk_Data_Mapping_Edit_Custom',data.wait)
            .click('@lnk_Data_Mapping_Edit_Custom')
        Administration_Foundation.waitForElementVisible('@lnk_SourcetoTargetMapping',data.wait)

        Administration_Foundation.waitForElementVisible('@lnk_Close_Button',data.wait)
            .click('@lnk_Close_Button')
        Administration_Foundation.waitForElementVisible('@lnk_Home_Page',data.wait)
            .click('@lnk_Home_Page')
        Administration_Foundation.waitForElementVisible('@lnk_HRImport_Link',data.wait)
            .click('@lnk_HRImport_Link')
        Administration_Foundation.waitForElementVisible('@lnk_ACME_0200',data.wait)
            .click('@lnk_ACME_0200')
        Administration_Foundation.waitForElementVisible('@lnk_Data_Mapping_Edit_Custom',data.wait)
            .click('@lnk_Data_Mapping_Edit_Custom')
        Administration_Foundation.waitForElementVisible('@lnk_SourcetoTargetMapping',data.wait)

        Administration_Foundation.waitForElementVisible('@lnk_Close_Button',data.wait)
            .click('@lnk_Close_Button')
        Administration_Foundation.waitForElementVisible('@lnk_Home_Page',data.wait)
            .click('@lnk_Home_Page')
        Administration_Foundation.waitForElementVisible('@lnk_HRImport_Link',data.wait)
            .click('@lnk_HRImport_Link')
    });

    this.When(/^Check if the user can view the 'Valid values and decode' screen$/, function () {
        Administration_Foundation.waitForElementVisible('@lnk_ACME_0100',data.wait)
            .click('@lnk_ACME_0100')
        Administration_Foundation.waitForElementVisible('@lnk_ValidValuesDecodes',data.wait)
            .click('@lnk_ValidValuesDecodes')
        Administration_Foundation.waitForElementVisible('@lnk_ValidValuesDecodes_text',data.wait)
            .click('@lnk_ValidValuesDecodes_text')
        Administration_Foundation.waitForElementVisible('@lnk_Close_Button',data.wait)
            .click('@lnk_Close_Button')

    });
    this.When(/^Check if the user can view the 'effective date derivation' screen$/, function () {
        Administration_Foundation.waitForElementVisible('@lnk_EffectiveDateDerivation', data.wait)
            .click('@lnk_EffectiveDateDerivation')
        Administration_Foundation.waitForElementVisible('@lnk_EffectiveDateDerivation_text', data.wait)
        Administration_Foundation.waitForElementVisible('@lnk_Close_Button',data.wait)
            .click('@lnk_Close_Button')

    });

    this.When(/^Check if the user can view the 'Data validation' page$/, function () {
        Administration_Foundation.waitForElementVisible('@lnk_DataValidation', data.wait)
            .click('@lnk_DataValidation')
        Administration_Foundation.waitForElementVisible('@lnk_DataValidation_text', data.wait)
        Administration_Foundation.waitForElementVisible('@lnk_Close_Button',data.wait)
            .click('@lnk_Close_Button')
    });
    this.When(/^Check if the user can view the 'Data derivation' screen$/, function () {
        Administration_Foundation.waitForElementVisible('@lnk_DataDerivation', data.wait)
            .click('@lnk_DataDerivation')
        Administration_Foundation.waitForElementVisible('@lnk_DataDerivation_text', data.wait)
        Administration_Foundation.waitForElementVisible('@lnk_Close_Button',data.wait)
            .click('@lnk_Close_Button')
        Administration_Foundation.waitForElementVisible('@lnk_Home_Page',data.wait)
            .click('@lnk_Home_Page')

    });
    this.When(/^Check that the user do not have selection on Administration tab$/, function () {
        Administration_Foundation.waitForElementNotPresent('@lnk_Administration_BH',data.wait);
        Administration_Foundation.waitForElementVisible('@lnk_HRImport_Link',data.wait)
            .click('@lnk_HRImport_Link')
            });

    this.When(/^Check the user is able to view the Event trigger configuration$/, function () {
        Administration_Foundation.waitForElementVisible('@lnk_Event_Triggers',data.wait)
            .click('@lnk_Event_Triggers')
        Administration_Foundation.waitForElementVisible('@lnk_Event_Triggers_text',data.wait)
            });
    this.When(/^Verify that the Promote Button is visible and should be disabled$/, function () {
        Administration_Foundation.waitForElementVisible('@lnk_Event_Triggers',data.wait)
            .click('@lnk_Event_Triggers')
        Administration_Foundation.waitForElementVisible('@lnk_PromoteButton',data.wait)
            .click('@lnk_PromoteButton')
        Administration_Foundation.waitForElementVisible('@lnk_Promote_DEV_Test',data.wait)
            .click('@lnk_Promote_DEV_Test')
    });

    this.When(/^User selects HUB_HRI_Config.Promote_PROD_Role and click on save$/, function () {
        Administration_Foundation.waitForElementVisible('@lnk_HUB_HRI_Config_Promote_PROD',data.wait)
            .click('@lnk_HUB_HRI_Config_Promote_PROD');
        Administration_Foundation.waitForElementVisible('@lnk_save_role',data.wait)
            .click('@lnk_save_role');

    });
    this.When(/^User selects HUB_HRI_Admin.Manager role and click on save$/, function () {
        Administration_Foundation.waitForElementVisible('@lnk_HUB_HRI_Admin_Manager',data.wait)
            .click('@lnk_HUB_HRI_Admin_Manager');
        Administration_Foundation.waitForElementVisible('@lnk_save_role',data.wait)
            .click('@lnk_save_role');

    });

    this.When(/^Check that the user is not able to view any configuration component$/, function () {
        Administration_Foundation.waitForElementNotPresent('@lnk_Configuration',data.wait);
        Administration_Foundation.waitForElementVisible('@lnk_HRImport_Link',data.wait)
            .click('@lnk_HRImport_Link');
    });

    this.When(/^Check that the user can view File Dashboard File breakdown and do error remediation and view Participant List tab$/, function () {
        Administration_Foundation.waitForElementVisible('@lnk_Time_period_Choose_one',data.wait)
            .click('@lnk_Time_period_Choose_one');
        Administration_Foundation.waitForElementVisible('@lnk_last_365_days',data.wait)
            .click('@lnk_last_365_days');
        Administration_Foundation.waitForElementVisible('@lnk_Search_button',data.wait)
            .click('@lnk_Search_button');
        Administration_Foundation.waitForElementVisible('@lnk_Search_text',data.wait)
            .setValue('@lnk_Search_text',data.FileID);
        Administration_Foundation.waitForElementVisible('@FileID8000042',data.wait)
            .click('@FileID8000042');
        Administration_Foundation.waitForElementVisible('@lnk_File_Breakdown',data.wait);
        Administration_Foundation.waitForElementVisible('@lnk_Error_and_Warning_Details',data.wait)
            .click('@lnk_Error_and_Warning_Details');
        //        var expected="HR Import - Error and Warning Details";
        // Administration_Foundation.getText('@lnk_Error_and_Warning_Details_page',function (actual) {
        // console.log(actual) ;
        // })
        // Administration_Foundation.assert.equal(actual.value,expected);
        Administration_Foundation.waitForElementVisible('@lnk_Error_and_Warning_Details_text',data.wait)

        Administration_Foundation.waitForElementVisible('@lnk_Participant_List',data.wait)
            .click('@lnk_Participant_List');
        Administration_Foundation.waitForElementVisible('@lnk_Participant_List_page',data.wait)
        Administration_Foundation.waitForElementVisible('@lnk_Error_and_Warning_Details',data.wait)
            .click('@lnk_Error_and_Warning_Details');
        Administration_Foundation.waitForElementVisible('@lnk_Approve_symbol',data.wait)
            .click('@lnk_Approve_symbol');
        Administration_Foundation.waitForElementVisible('@lnk_Approve_button',data.wait)
            .click('@lnk_Approve_button');
        Administration_Foundation.waitForElementVisible('@lnk_Time_period_Choose_one',data.wait)
            .click('@lnk_Time_period_Choose_one');
        Administration_Foundation.waitForElementVisible('@lnk_last_365_days',data.wait)
            .click('@lnk_last_365_days');
        Administration_Foundation.waitForElementVisible('@lnk_Search_button',data.wait)
            .click('@lnk_Search_button');
        Administration_Foundation.waitForElementVisible('@lnk_Search_text',data.wait)
            .setValue('@lnk_Search_text',data.FileID);
        Administration_Foundation.waitForElementVisible('@FileID8000042',data.wait)
            .click('@FileID8000042');
        Administration_Foundation.waitForElementVisible('@lnk_Error_and_Warning_Details',data.wait)
            .click('@lnk_Error_and_Warning_Details');
        Administration_Foundation.waitForElementVisible('@Checkbox',data.wait)
            .click('@Checkbox');
        Administration_Foundation.waitForElementVisible('@Text_Error1',data.wait)
            .clearValue('@Text_Error1').setValue('@Text_Error1',data.Correct_Value);
                Administration_Foundation.waitForElementVisible('@Text_Error2',data.wait)
                    .clearValue('@Text_Error2').setValue('@Text_Error2',data.Correct_Value);
        Administration_Foundation.waitForElementVisible('@SaveAndSubmitforPeerReview',data.wait)
            .click('@SaveAndSubmitforPeerReview');


    });

    this.When(/^User assigns the same role for scenario 6$/, function () {
        browser = this;
        var AFURL;
        //   AFURL = data.AFURL;
        initializePageObjects(browser, function () {
            browser.maximizeWindow()
                .deleteCookies()
                .url(data.AFURL);
            browser.timeoutsImplicitWait(30000);
        });
        Administration_Foundation.waitForElementVisible('@lnk_Administration', data.wait)
            .click('@lnk_Administration')
        Administration_Foundation.waitForElementVisible('@lnk_Manage_Administrator', data.wait)
            .click('@lnk_Manage_Administrator')
        Administration_Foundation.waitForElementVisible('@lnk_text_box', data.wait)
            .setValue('@lnk_text_box', data.Name);
        Administration_Foundation.waitForElementVisible('@lnk_find_now', data.wait)
            .click('@lnk_find_now');
        Administration_Foundation.waitForElementVisible('@lnk_User_Name', data.wait)
            .click('@lnk_User_Name')
        Administration_Foundation.waitForElementVisible('@lnk_Role_edit', data.wait)
            .click('@lnk_Role_edit')
        Administration_Foundation.waitForElementVisible('@lnk_HUB_HRI_Config_Read_Only_Role', data.wait)
            .click('@lnk_HUB_HRI_Config_Read_Only_Role');
        Administration_Foundation.waitForElementVisible('@lnk_save_role', data.wait)
            .click('@lnk_save_role');
    });
    this.When(/^User assigns the same role  for scenario 1$/, function () {
        browser = this;
        var AFURL;
        //   AFURL = data.AFURL;
        initializePageObjects(browser, function () {
            browser.maximizeWindow()
                .deleteCookies()
                .url(data.AFURL);
            browser.timeoutsImplicitWait(30000);
        });
        Administration_Foundation.waitForElementVisible('@lnk_Administration', data.wait)
            .click('@lnk_Administration')
        Administration_Foundation.waitForElementVisible('@lnk_Manage_Administrator', data.wait)
            .click('@lnk_Manage_Administrator')
        Administration_Foundation.waitForElementVisible('@lnk_text_box', data.wait)
            .setValue('@lnk_text_box', data.Name);
        Administration_Foundation.waitForElementVisible('@lnk_find_now', data.wait)
            .click('@lnk_find_now');
        Administration_Foundation.waitForElementVisible('@lnk_User_Name', data.wait)
            .click('@lnk_User_Name')
        Administration_Foundation.waitForElementVisible('@lnk_Role_edit', data.wait)
            .click('@lnk_Role_edit')
        Administration_Foundation.waitForElementVisible('@lnk_HUB_HRI_Admin_Analyst',data.wait)
            .click('@lnk_HUB_HRI_Admin_Analyst');
        Administration_Foundation.waitForElementVisible('@lnk_save_role', data.wait)
            .click('@lnk_save_role');
    });
    this.When(/^User selects HUB_HRI_Admin.Analyst role and click on save$/, function () {
        Administration_Foundation.waitForElementVisible('@lnk_HUB_HRI_Admin_Analyst',data.wait)
            .click('@lnk_HUB_HRI_Admin_Analyst');
        Administration_Foundation.waitForElementVisible('@lnk_save_role', data.wait)
            .click('@lnk_save_role');
                //browser.pause(7000);
    });
    this.When(/^User assigns the same role  for scenario 2$/, function () {
        browser = this;
        var AFURL;
        //   AFURL = data.AFURL;
        initializePageObjects(browser, function () {
            browser.maximizeWindow()
                .deleteCookies()
                .url(data.AFURL);
            browser.timeoutsImplicitWait(30000);
        });
        Administration_Foundation.waitForElementVisible('@lnk_Administration', data.wait)
            .click('@lnk_Administration')
        Administration_Foundation.waitForElementVisible('@lnk_Manage_Administrator', data.wait)
            .click('@lnk_Manage_Administrator')
        Administration_Foundation.waitForElementVisible('@lnk_text_box', data.wait)
            .setValue('@lnk_text_box', data.Name);
        Administration_Foundation.waitForElementVisible('@lnk_find_now', data.wait)
            .click('@lnk_find_now');
        Administration_Foundation.waitForElementVisible('@lnk_User_Name', data.wait)
            .click('@lnk_User_Name')
        Administration_Foundation.waitForElementVisible('@lnk_Role_edit', data.wait)
            .click('@lnk_Role_edit')
        Administration_Foundation.waitForElementVisible('@lnk_HUB_HRI_Admin_Analyst',data.wait)
            .click('@lnk_HUB_HRI_Admin_Analyst');
        Administration_Foundation.waitForElementVisible('@lnk_save_role',data.wait)
            .click('@lnk_save_role');
    });
    this.When(/^User selects HUB_HRI_Admin.Read_Only Role and click on save$/, function () {
        Administration_Foundation.waitForElementVisible('@lnk_HUB_HRI_Admin_Read_Only', data.wait)
             .click('@lnk_HUB_HRI_Admin_Read_Only');
        browser.pause(5000);
        Administration_Foundation.waitForElementVisible('@lnk_save_role', data.wait)
            .click('@lnk_save_role');
    });
    this.When(/^Check that the user is able to view File Dashboard$/, function () {
        Administration_Foundation.waitForElementVisible('@lnk_File_Dashboard', data.wait);
        Administration_Foundation.waitForElementVisible('@lnk_file_id', data.wait)
            .click('@lnk_file_id');

    });
    this.When(/^Check that the user can view the File breakdown page$/, function () {
        Administration_Foundation.waitForElementVisible('@lnk_File_breakdown_text', data.wait);
            });
    this.When(/^User assigns the same role  for scenario 3$/, function () {
        browser = this;
        var AFURL;
        //   AFURL = data.AFURL;
        initializePageObjects(browser, function () {
            browser.maximizeWindow()
                .deleteCookies()
                .url(data.AFURL);
            browser.timeoutsImplicitWait(30000);
        });
        Administration_Foundation.waitForElementVisible('@lnk_Administration', data.wait)
            .click('@lnk_Administration')
        Administration_Foundation.waitForElementVisible('@lnk_Manage_Administrator', data.wait)
            .click('@lnk_Manage_Administrator')
        Administration_Foundation.waitForElementVisible('@lnk_text_box', data.wait)
            .setValue('@lnk_text_box', data.Name);
        Administration_Foundation.waitForElementVisible('@lnk_find_now', data.wait)
            .click('@lnk_find_now');
        Administration_Foundation.waitForElementVisible('@lnk_User_Name', data.wait)
            .click('@lnk_User_Name')
        Administration_Foundation.waitForElementVisible('@lnk_Role_edit', data.wait)
            .click('@lnk_Role_edit')
        Administration_Foundation.waitForElementVisible('@lnk_HUB_HRI_Admin_Read_Only',data.wait)
            .click('@lnk_HUB_HRI_Admin_Read_Only')
        Administration_Foundation.waitForElementVisible('@lnk_save_role',data.wait)
            .click('@lnk_save_role');
    });

    this.When(/^User selects HUB_HRI_Config.Client_File_Setup Role and click on save$/, function () {
        Administration_Foundation.waitForElementVisible('@lnk_HUB_HRI_Config_Client_File_Setup', data.wait)
            .click('@lnk_HUB_HRI_Config_Client_File_Setup');
        Administration_Foundation.waitForElementVisible('@lnk_save_role', data.wait)
            .click('@lnk_save_role');
    });
    this.When(/^User should see confirmation message$/, function () {
        Administration_Foundation.waitForElementPresent('@lnk_AF_confirmation_msg', data.wait);
        Administration_Foundation.getText('@lnk_AF_confirmation_msg', function(actual) {
        console.log(actual);
        })


    });
    this.When(/^User clicks on Save Button$/, function () {
        browser.useXpath()
       Administration_Foundation.waitForElementVisible('@lnk_save_role', data.wait)
        .click('@lnk_save_role');
    });
    this.When(/^Verify that The Add button on the extreme right to 'Add from Library' and 'Add a New Client Specific Group' is not visible$/, function () {
        Administration_Foundation.waitForElementVisible('@lnk_file_config_list', data.wait)
            .click('@lnk_file_config_list');
        Administration_Foundation.waitForElementVisible('@lnk_Add_button', data.wait)
            .click('@lnk_Add_button');
    });

    this.When(/^User assigns the same role  for scenario 5$/, function () {
        browser = this;
        var AFURL;
        //   AFURL = data.AFURL;
        initializePageObjects(browser, function () {
            browser.maximizeWindow()
                .deleteCookies()
                .url(data.AFURL);
            browser.timeoutsImplicitWait(30000);
        });
        Administration_Foundation.waitForElementVisible('@lnk_Administration', data.wait)
            .click('@lnk_Administration')
        Administration_Foundation.waitForElementVisible('@lnk_Manage_Administrator', data.wait)
            .click('@lnk_Manage_Administrator')
        Administration_Foundation.waitForElementVisible('@lnk_text_box', data.wait)
            .setValue('@lnk_text_box', data.Name);
        Administration_Foundation.waitForElementVisible('@lnk_find_now', data.wait)
            .click('@lnk_find_now');
        Administration_Foundation.waitForElementVisible('@lnk_User_Name', data.wait)
            .click('@lnk_User_Name')
        Administration_Foundation.waitForElementVisible('@lnk_Role_edit', data.wait)
            .click('@lnk_Role_edit')
        Administration_Foundation.waitForElementVisible('@lnk_HUB_HRI_Config_Promote_PROD',data.wait)
            .click('@lnk_HUB_HRI_Config_Promote_PROD')
        Administration_Foundation.waitForElementVisible('@lnk_save_role',data.wait)
            .click('@lnk_save_role');
    });
    this.When(/^Verify that the Promote Button is visible$/, function () {
        Administration_Foundation.waitForElementVisible('@lnk_Event_Triggers', data.wait)
            .click('@lnk_Event_Triggers')
        Administration_Foundation.waitForElementVisible('@lnk_PromoteButton', data.wait)
            .click('@lnk_PromoteButton')
    });
    this.When(/^User click on the Promote button and promote between environment$/, function () {
        Administration_Foundation.waitForElementVisible('@lnk_Promote_Test_CSO',data.wait)
            .click('@lnk_Promote_Test_CSO')
        Administration_Foundation.waitForElementVisible('@lnk_isupport_text_box',data.wait)
            .setValue('@lnk_isupport_text_box',data.Ticket_Number)
        Administration_Foundation.waitForElementVisible('@lnk_checkbox_promote',data.wait)
            .click('@lnk_checkbox_promote')
        Administration_Foundation.waitForElementVisible('@lnk_Run_now_button',data.longwait)
            .click('@lnk_Run_now_button')

        Administration_Foundation.waitForElementVisible('@lnk_promote_successfull',data.longwait)
            .click('@lnk_promote_successfull')
    });
    this.When(/^User assigns the same role  for scenario 4$/, function () {
        browser = this;
        var AFURL;
        //   AFURL = data.AFURL;
        initializePageObjects(browser, function () {
            browser.maximizeWindow()
                .deleteCookies()
                .url(data.AFURL);
            browser.timeoutsImplicitWait(30000);
        });
        Administration_Foundation.waitForElementVisible('@lnk_Administration', data.wait)
            .click('@lnk_Administration')
        Administration_Foundation.waitForElementVisible('@lnk_Manage_Administrator', data.wait)
            .click('@lnk_Manage_Administrator')
        Administration_Foundation.waitForElementVisible('@lnk_text_box', data.wait)
            .setValue('@lnk_text_box', data.Name);
        Administration_Foundation.waitForElementVisible('@lnk_find_now', data.wait)
            .click('@lnk_find_now');
        Administration_Foundation.waitForElementVisible('@lnk_User_Name', data.wait)
            .click('@lnk_User_Name')
        Administration_Foundation.waitForElementVisible('@lnk_Role_edit', data.wait)
            .click('@lnk_Role_edit')
        Administration_Foundation.waitForElementVisible('@lnk_HUB_HRI_Config_Client_File_Setup',data.wait)
            .click('@lnk_HUB_HRI_Config_Client_File_Setup')
        Administration_Foundation.waitForElementVisible('@lnk_save_role',data.wait)
            .click('@lnk_save_role');
    });
    this.When(/^Check if the user can view the Data mapping page for both SDLF and Custom files$/, function () {
        Administration_Foundation.waitForElementVisible('@lnk_ACME_0100', data.wait)
            .click('@lnk_ACME_0100')
        Administration_Foundation.waitForElementVisible('@lnk_Data_Mapping_Edit_Custom',data.wait)
            .click('@lnk_Data_Mapping_Edit_Custom')
        Administration_Foundation.waitForElementVisible('@lnk_SourcetoTargetMapping',data.wait)
            .click('@lnk_SourcetoTargetMapping')
        Administration_Foundation.waitForElementVisible('@lnk_Cancel_button',data.wait)
            .click('@lnk_Cancel_button')
        Administration_Foundation.waitForElementVisible('@lnk_Home_Page',data.wait)
            .click('@lnk_Home_Page')
        Administration_Foundation.waitForElementVisible('@lnk_HRImport_Link',data.wait)
            .click('@lnk_HRImport_Link')
        Administration_Foundation.waitForElementVisible('@lnk_ACME_0200',data.wait)
            .click('@lnk_ACME_0200')
        Administration_Foundation.waitForElementVisible('@lnk_Data_Mapping_Edit_Custom',data.wait)
            .click('@lnk_Data_Mapping_Edit_Custom')
        Administration_Foundation.waitForElementVisible('@lnk_SourcetoTargetMapping',data.wait)

        Administration_Foundation.waitForElementVisible('@lnk_Cancel_button',data.wait)
            .click('@lnk_Cancel_button')
        Administration_Foundation.waitForElementVisible('@lnk_Home_Page',data.wait)
            .click('@lnk_Home_Page')
    });
    this.When(/^User clicks on HR Import Link$/, function () {
        Administration_Foundation.waitForElementVisible('@lnk_Configuration',data.wait)
        Administration_Foundation.waitForElementVisible('@lnk_HRImport_Link',data.wait)
            .click('@lnk_HRImport_Link')
    });
    this.When(/^User clicks on Edit button under Groups$/, function () {
                Administration_Foundation.waitForElementVisible('@lnk_Edit_Group',data.wait)
            .click('@lnk_Edit_Group')
    });
    this.When(/^User clicks on QA-Analyst-TEST-CLIENTS group$/, function () {
        Administration_Foundation.waitForElementVisible('@lnk_QA_Analyst_TEST_CLIENTS_role',data.wait)
            .click('@lnk_QA_Analyst_TEST_CLIENTS_role')
    });
    this.When(/^User clicks on QA-TL or Mgr-All except MMC group$/, function () {
        Administration_Foundation.waitForElementVisible('@lnk_QA_TL_MgrAllexceptMMC_role',data.wait)
            .click('@lnk_QA_TL_MgrAllexceptMMC_role')
    });
    this.When(/^User clicks on Save Button for Group$/, function () {
        Administration_Foundation.waitForElementVisible('@lnk_Save_Group',data.wait)
            .click('@lnk_Save_Group')
    });

    // this.When(/^User click on Client name to navigate to the homepage for scenario 6$/, function () {
    //     Administration_Foundation.waitForElementVisible('@lnk_client_MercerMarketplace365Plus', data.wait)
    //         .click('@lnk_client_MercerMarketplace365Plus');
    // });
    this.When(/^User selects HUB_HRI_Admin.Analyst role$/, function () {
        Administration_Foundation.waitForElementVisible('@lnk_HUB_HRI_Admin_Analyst',data.wait)
            .click('@lnk_HUB_HRI_Admin_Analyst');
        Administration_Foundation.waitForElementVisible('@lnk_save_role', data.wait)
            .click('@lnk_save_role');
        //browser.pause(7000);
    });
    this.When(/^User is able to remediate the file$/, function () {

        Administration_Foundation.waitForElementVisible('@AddEvidenceButton',data.wait)
            .click('@AddEvidenceButton');
        browser.pause(5000);
        browser.useXpath().click("//h3[contains(text(),'Evidence')]/div/i");
       // Administration_Foundation.waitForElementVisible('@Add_Document',data.wait)
          //  .click('@Add_Document');
        Administration_Foundation.waitForElementVisible('@EvidenceType',data.wait)
            .click('@EvidenceType');
        Administration_Foundation.waitForElementVisible('@EvidenceOption',data.wait)
            .click('@EvidenceOption');
        Administration_Foundation.waitForElementVisible('@DocumentLocation',data.wait)
            .setValue('@DocumentLocation',data.documentloc);
        Administration_Foundation.waitForElementVisible('@Comments',data.wait)
            .setValue('@Comments',data.comments);
        Administration_Foundation.waitForElementVisible('@Add_Button',data.wait)
            .click('@Add_Button');
        Administration_Foundation.waitForElementVisible('@Error_link',data.wait)
            .click('@Error_link');
        Administration_Foundation.waitForElementVisible('@Checkbox',data.wait)
            .click('@Checkbox');
        Administration_Foundation.waitForElementVisible('@SaveAndSubmitforPeerReview',data.wait)
            .click('@SaveAndSubmitforPeerReview');
        Administration_Foundation.waitForElementVisible('@SubmitButtonforPR',data.longwait)
            .click('@SubmitButtonforPR');
        Administration_Foundation.waitForElementVisible('@CheckMessage',data.longwait);

    });
}








